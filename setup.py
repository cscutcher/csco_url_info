from setuptools import setup, find_packages
setup(
    name="csco_url_info",
    description="Checks URL against malware DB.",
    author="Chris Scutcher",
    author_email="chris@scutcher.uk",
    packages=find_packages(),
    use_scm_version={
        "version_scheme": "python-simplified-semver",
        "write_to": "dist/VERSION.txt",
    },
    setup_requires=['setuptools_scm'],
    install_requires=[
        "aiohttp",
        "cement",
        "pytest",
        "redis-py-cluster",
    ],
    extras_require={
        "dev": [
            "pytest-aiohttp",
            "pytest-asyncio",
            "pylama",
            "pytest-cov",
            "pytest-html",
            "pytest-benchmark[histogram]",
        ],
    },
    entry_points={
        "console_scripts": [
            "csco-url-info = csco_url_info.app:main",
        ]
    },
    python_requires=">=3.6",
)
