# URL Info
This code is for an exercise.

[![pipeline status](https://gitlab.com/cscutcher/csco_url_info/badges/master/pipeline.svg)](https://gitlab.com/cscutcher/csco_url_info/commits/master)
[![coverage report](https://gitlab.com/cscutcher/csco_url_info/badges/master/coverage.svg)](https://gitlab.com/cscutcher/csco_url_info/commits/master)


# Problem Statement

We have an HTTP proxy that is scanning traffic looking for malware URLs.
Before allowing HTTP connections to be made, this proxy asks a service that
maintains several databases of malware URLs if the resource being requested is
known to contain malware.

Write a small web service, in the language/framework your choice, that responds
to GET requests where the caller passes in a URL and the service responds with
some information about that URL.

The GET requests look like this:

```
GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}
```

The caller wants to know if it is safe to access that URL or not.

As the implementer, you get to choose the response format and structure.
These lookups are blocking users from accessing the URL until the caller
receives a response from your service.

Give some thought to the following:

* P0: The size of the URL list could grow infinitely, how might you scale this
  beyond the memory capacity of this VM? Bonus if you implement this.
* P1: The number of requests may exceed the capacity of this VM, how might you
  solve that? Bonus if you implement this.
* P2: What are some strategies you might use to update the service with new
  URLs?
  Updates may be as much as 5 thousand URLs a day with updates arriving every
  10 minutes.
* P3: Bonus points if you containerize the app.


# Installation

```bash
git clone git@gitlab.com:cscutcher/csco_url_info.git
pip install csco_url_info
```

# Environmental Variables

### `CSCO_REDIS_CLUSTER`

Comma seperated list of redis nodes for cluster discovery.
Each node should include hostname and port seperated by `:`.

For example; `CSCO_REDIS_CLUSTER="localhost:7000"`

### `CSCO_LISTEN_PORT`

Port for server to listen on when running via `csco-url-info` command.
If absent then defaults to 8080.

For example; `CSCO_LISTEN_PORT="80"`

### `CSCO_REDIS_PASS`

Password for REDIS server.

### `CSCO_URL_INFO_DB`

DB type to use for server. See `url_info_db.py` for available dbs.
Defaults to `DumbDatabase` which is non-persistent, in memory only.
Set to `RedisClusterDatabase` to use Redis cluster.

# Run server

```bash
csco-url-info
```

# Tests

Install additional dev requirements. Assuming checked out to `csco_url_info`

```bash
cd csco_url_info
pip install -e .[dev]
pytest
```

# Building docker image

```bash
python setup.py bdist_wheel
docker build -t csco_url_info:latest .
```

# Deploy update helm

This is still a work in progress.

```bash
helm upgrade --install staging helm/csco-url-info/
```

# Design thinking and dev diary

See [CHANGELOG.md](CHANGELOG.md) for notes on design thinking and dev diary.

