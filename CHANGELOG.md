# Changelog and Dev Diary

# Problem Statement

We have an HTTP proxy that is scanning traffic looking for malware URLs.
Before allowing HTTP connections to be made, this proxy asks a service that
maintains several databases of malware URLs if the resource being requested is
known to contain malware.

Write a small web service, in the language/framework your choice, that responds
to GET requests where the caller passes in a URL and the service responds with
some information about that URL.

The GET requests look like this:

```
GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}
```

The caller wants to know if it is safe to access that URL or not.

As the implementer, you get to choose the response format and structure.
These lookups are blocking users from accessing the URL until the caller
receives a response from your service.

Give some thought to the following:

* P0: The size of the URL list could grow infinitely, how might you scale this
  beyond the memory capacity of this VM? Bonus if you implement this.
* P1: The number of requests may exceed the capacity of this VM, how might you
  solve that? Bonus if you implement this.
* P2: What are some strategies you might use to update the service with new
  URLs?
  Updates may be as much as 5 thousand URLs a day with updates arriving every
  10 minutes.
* P3: Bonus points if you containerize the app.

# Design Thinking

### Database

Well database decision is important, but we should be careful not to tie
ourselves into any database in particular at this point.

**D10: Do best to encapsulate away database from design.**

Given the relatively simplistic data being stored, and the speed requirements
a key-value store seems the way to go.

We've got a bunch of options in this space, Redis, Arangodb, Cassandra.

I'm going to go with Redis which allows sharding, replication and even partial
partition support, which should cover nicely P0 and P1.

The pipelining and local LUA support should make P2 easier.
All that being said I want to be careful not to lock in too early to a database
so will keep **D10** in mind.

**D20: Start with Redis as DB.**

It may be that if the service evolves to be more complex that the DB decision
changes, or that we move to Redis as a caching layer on top of something else.

### Implementation

Python for familiarity and it being god of the glue languages.
As the design evolves it's plausible we might want to move to another language
for speed.

**D30: Using python 3.6**

For Redis access that leaves us with `redis-py` and `redis-py-cluster`
as de facto standard libraries. I'll start with one and move to the other.

I'd really like to go for an async style design to better reflect the business
logic which might cause issues with `redis-py` which seems more thread driven.

While there is `asyncio-redis` available, there is no cluster equivalent, so I'm
going to avoid that for now.

**D40: Asyncio based**

Will use `aiohttp` for front-end, but that should be the easy bit.

**D45: Component driven**

I'm normally big on component driven approaches to designing something like
this. Given that we don't know how it's going to evolve and which decisions
we're going to keep or throw away it's important to make sure that we maintain
component boundaries to make it easy to test, compare and change as the product
evolves. Stock python and careful use of globals can give you that, but I in
the past I've been a big fan of a [zope.interface] and friends to achieve that.
I might take a bit of a risk with this and go with [cement] instead as I've
been looking for an excuse to try it. We'll see how it goes.

For containerizing, I'm going to go for docker-compose as I have more
familiarity with that. With more time I'd go for the Kubernetes route.

**D50: Use Docker-compose**

### API

Going to be careful to keep the API clear and simple to start with.
It's tempting to go beyond the spec but in my experience keeping the input and
output options as limited as possible until one **needs** to expand it is the
way to go.

Equally, I want to make the response as explicit as possible, so I'll go
for something simple like;

```json
{"danger": true}
```

**D60: Simple JSON based output.**

### More Ideas

* Improving speed;
    * HTTP pipelining / HTTP2 multiplexing
    * Make responses more efficient to parse with something like protobuf
    * Obviously load balance over multiple instances


# Dev Notes: 2018-04-26

* Got a bit bogged down in parsing IDN domain names. Shouldn't have been
  thinking about that yet! Arguable the matching in the REST routing is
  overkill, might want to scale it back a bit.
* Got some reasonable framework in to allow easy test-driven-development going
  forward. The random domain name generation is a bit dumb, but it'll do for
  now. Setup the test and coverage in CI so can get immediate feedback on next
  steps.
* GitLabs free runners were having a bad day, so quickly set up free GCE account
  for Kubernetes to host some dedicated GitLab CI runners.
  Given I've got that setup I may reconsider **D50** and use Kubernetes instead
  `docker-compose` for the container deploy. I'm less familiar with the former
  so we'll see how it goes.
* I'm really keen to get **D45** right and make it trivial to swap out different
  backend DB's easily to stay flexible easily do performance comparisons.
  `zope.interface` is my normal goto, but I wanted to look at `Cement`.

  After a bit of a play, I quite like it, but it's overkill for what I need
  here; it's a nice framework for building a CLI and handles config, args and
  components in a really holistic way, but it feels a bit better suited to a
  more complex CLI. `aiohttp` already gives quite a lot in the way of process
  level context management anyway.

### Next Steps

* Dumbest possible DB implementation. Probably just using a dictionary.
* **P2** in the problem statement indicates the importance of bulk updates.
  Will probably emphasise this when building DB interface.

### Questions for PO
* Is `dangerous` as a bool sufficient?
* What format are we expecting updates?
    ** Will we always get the complete hostname, port, path, qs combo for an
       update?
    ** Or might an update omit everything but the hostname in order to mark the
       whole domain as dodgy?
    ** Or, building on that, might we need to whitelist individual paths?
    ** Should records expire in any way?
* How much validation do we want on the APIs?

# Dev Notes: 2018-04-27

* Implemented dumb DB implementation using dictionary. This is enough to get
  some testing in so we can see how other DB's perform when compared.
* Implement the PUT api for updates. Tried to make it efficient for doing large
  numbers of updates at once by making full use of async.
* Implemented dockerfile so I can try deploying to GCE. Dockerfile is built as
  part of CI.
* While I'm obviously not done yet, the main problem is "solved" including
  P3 and part of P2.

### Random Test Data Generation

Stil getting bogged down in trying to generate a nice set of test data to work
with.

On one hand I wanted to get a reasonably tricky input to work with doing my
best to push the limits of the various specs.

However, I'm remembering how complex the spec can become, especially seen
through the lense of various libraries URL handling abilities.

This has made a bit of a mess of `test_utils`.
The algorithms are a bit aribtrary and don't test things as fully as I'd like.
Plus they're ugly as all hell.

Then again I don't want to sink any more time into them so it'll have to do for
now.

Maybe there's a good library that can generate some good random URLs.

### URL handling is a bit messy
There's too many ways of representing the URL for the query right now, and
it's resulting in inconsistent encoding at the API boundaries.

I think I've beaten it into submission, but I'd like to pick one of the
libraries implementations for a URL class, (either stdlib or yarl)
and be more regimented in sticking to that representation everywhere.

Ditto for picking a string representation. Might even tweak the API if that's
acceptible. It's awkward embedding the query URL in the path for the API
I rather not do it that way, but to change it more would require a more indepth
discussion with PO on what they want for the full solution.

### Next Steps

* Hook in Redis as database replacement.
* Deploy docker image to GCE kubernetes.
* Cluster the Redis databases.

If I can get those done I should be able to claim P0 and P1 as done.

Lower priority;

* Get some performance metrics.
* Would be nice to auto-generate some API documentation.
  Ideally documentation that I can execute for further testing.
* Fix API for missing records: It suddenly occured to me I should be returning
  404 not a record with nulls in! Doh!

# Dev Notes: 2018-05-01

* Implemented Redis cluster based backend in the code.
* Include testing against cluster backend in CI.
* Got some benchmarks of different backends compared. Unsurprisingly the redis
  is slower than the dict, but obviously the dict wont scale or persist.

### Helm / k8s deployment
Implemented a primitive helm deployment for k8s to GCE.
Helm is new to me so I was learning as I went.

The dict based deployment was easy and is available at;
  `http://csco-url-info.scutcher.uk/`

However adding the redis cluster into the deployment was more painful than I'd
hoped.
The blocking issue was that the `stable/redis` helm image, despite mentioning
clustering, is not a cluster in the same way as `redis-py-cluster` understands
and is instead just doing replication and load balancing.

This took irritatingly long to diagnose, and I'm out of time, so leaving it with
deployed with the dict backend for now.

I believe I've got load balancing working for the actual rest-api application,
but without the redis backend it's not much use as each node will have a
different dataset.

To get redis working properly I'll either have to switch to using the
non-clustered client in the code, which should be trivial enough, or write a
helm file to spin up a proper Redis Cluster (as there doesn't seem to be one
available already).

# Summary

There's obviously loads more stuff I could do to expand on the exercise.

* The core concept is implemented, and it's tested working with multiple
  backends.
* It deploys to a GCE k8s cluster via helm, even if not in its fully scalable
  mode.
* The framework is sound, with CI and reasonable test coverage to start with.
  I'd obviously like to expand and cleanup before going into production but
  its a solid foundation.

Some obvious stuff I've realised I'm missing, now that I'm out of time;

* The full redis deployment as mentioned above.
* The API should be returning 404s when records are missing.
* I should have written a test that actually times how long 5000
  takes to insert. I have a few tests that do 100 at a time, including
  the benchmark, but it'd be nice to get the runtime for that given it's
  mentioned in the exercise description.
* I didn't implement partial lookups. I'd planned to do this in a relatively
  simple way to start with by doing multiple lookups to redis at once.
  I think this should be sufficiently efficient given the ability to pipeline
  redis requests.


[zope.interface]: https://github.com/zopefoundation/zope.interface
[cement]: http://builtoncement.com/
