# -*- coding: utf-8 -*-
"""
Utilities for helping with tests.

TODO:
    The random generation in here is a complete mess.
    The algorithms attempt to stretch the spec, but I've taken some shortcuts
    and it's definitely not efficient code!
    Needs a cleanup!
    Maybe there's some good libraries for random URL generation.

"""
from contextlib import contextmanager
from pprint import pformat
from urllib.parse import quote as url_quote
from urllib.parse import urlencode
import logging
import random
import string

from csco_url_info.url_info import URLInfo

DEV_LOGGER = logging.getLogger(__name__)

TEST_IDN_TLDS = (
    'xn--kgbechtv',
    'xn--hgbk6aj7f53bba',
    'xn--0zwm56d',
    'xn--g6w251d',
    'xn--80akhbyknj4f',
    'xn--11b5bs3a9aj6g',
    'xn--jxalpdlp',
    'xn--9t4b11yi5a',
    'xn--deba0ad',
    'xn--zckzah',
    'xn--hlcj6aya9esc7a',
)

VALID_ASCII_LABEL_CHARS = string.ascii_lowercase + string.digits
VISIBLE_ASCII_CHARS = (
    string.ascii_letters +
    string.digits +
    string.punctuation
)


def get_random_string(length, chars=(string.printable)):
    """
    Get a random string.
    """
    return "".join(
        random.choice(chars) for _ in range(length))


def get_random_domain_label(length):
    """
    Get random domain label within ascii range.
    """
    return get_random_string(length, VALID_ASCII_LABEL_CHARS)


def get_random_domain_name(
        idn_chance=0.5,
        length_chance=0.2,
        max_length=253,
        max_label_length=63):
    """
    Generate random domain name for testing purposes.

    :param idn_chance:
        Chance that we'll use a IDN TLD. Float 0.0 -> 1.0

    :param length_chance:
        Chance after adding each label to domain that we'll stop adding labels.
        Setting this higher will result in shorter domain names.
        Float 0.0 -> 1.0

    :param max_length:
        Maximum size of domain name. Default is from the spec.

    :param max_label_length:
        Maximum size of each label in domain name. Default is from the spec.

    TODO:
        Would be nice to generate a completely random domains using all
        possible IDN domain characters, but that would overcomplicate this
        further.
    """
    chars_remaining = max_length
    domain_labels = []

    # Sometimes use IDN TLD
    if random.random() < idn_chance:
        tld = random.choice(TEST_IDN_TLDS)
        domain_labels.append(tld)
        chars_remaining -= len(tld) + 1

    while chars_remaining > 0:
        label_length = random.randint(
            1,
            min(chars_remaining, max_label_length)
        )
        domain_labels.append(get_random_domain_label(label_length))
        chars_remaining -= label_length + 1

        if random.random() < length_chance:
            break

    domain_name = ".".join(reversed(domain_labels))
    return domain_name


def get_random_path_component(length):
    """
    Get random path component.
    """
    initial_string = get_random_string(length, string.ascii_letters)
    return url_quote(initial_string)


def get_random_uri_path(
        empty_chance=0.2, length_chance=0.8, max_length=1000):
    """
    Generate random domain name for testing purposes.

    :param empty_chance:
        Chance of getting empty path.

    :param length_chance:
        Chance after adding each component to path that we'll stop adding.
        Setting this higher will result in shorter paths.
        Float 0.0 -> 1.0

    :param max_length:
        This doesn't set an abosolute limit but will stop growing the path
        once it exceeeds this size.
    """
    if random.random() < empty_chance:
        return ""

    chars_remaining = max_length
    components = []

    while chars_remaining > 0:
        components.append(
            get_random_path_component(random.randint(1, chars_remaining)))
        chars_remaining -= len(components[-1]) + 1
        if random.random() < length_chance:
            break

    return "/".join(components)


def get_random_uri_query_string(
        empty_chance=0.4, length_chance=0.8, max_length=1000):
    """
    Generate random domain name for testing purposes.

    :param empty_chance:
        Chance of getting empty path.

    :param length_chance:
        Chance after adding each pair to QS that we'll stop adding.
        Setting this higher will result in shorter query strings.
        Float 0.0 -> 1.0

    :param max_length:
        This doesn't set an abosolute limit but will stop growing the QS
        once it exceeeds this size.
    """
    if random.random() < empty_chance:
        return ""

    query_data = {}
    query_string = urlencode(query_data)
    query_string_len = len(query_string)

    while len(query_string) < max_length:
        # This is pretty bloody arbitary I know! It'll do for now!
        max_key_value_length = int(
            max(1, ((max_length - query_string_len) / 10)))

        key = get_random_string(
            random.randint(1, max_key_value_length),
            string.ascii_letters,
        )
        value = get_random_string(
            random.randint(1, max_key_value_length),
            string.ascii_letters,
        )

        query_data[key] = value
        query_string = urlencode(query_data)
        query_string_len = len(query_string)

        if random.random() < length_chance:
            break

    return query_string


def get_random_port(none_chance=0.5):
    """
    Get a random port number or None.

    :param none_chance:
        Chance of returning None

    :return int or None:
    """
    include_port = random.random() < none_chance
    if include_port:
        return random.randint(0, 65535)
    else:
        return None


RANDOMISH_SEED = "dangermouse"
_RANDOMISH_STATE = None


@contextmanager
def randomish():
    """
    Context manager that allows for tests that are randomish.

    That is to say that one can generate a number of random test cases, but
    each run of the test will generate the same random cases.

    This is useful for generating large numbers of interesting test cases but
    without introducing intermittent test failures.

    RANDOMISH_SEED above can be changed semi-reguarly in order to shake the
    system up and see if we get different results.

    The scope being set to "session" means that this fixture will apply to the
    entire test session.
    """
    global _RANDOMISH_STATE

    original_state = random.getstate()
    if _RANDOMISH_STATE is None:
        random.seed(RANDOMISH_SEED)
    else:
        random.setstate(_RANDOMISH_STATE)
    yield

    _RANDOMISH_STATE = random.getstate()
    random.setstate(original_state)


async def assert_url_record_exists(client, app, url_key, url_info):
    """
    Helper function to assert key and info exists in database by calling API.
    """
    port = url_key.port
    url = get_url_from_url_key(app, url_key)
    DEV_LOGGER.debug("Looking up: %r @ %r", url_key, url)
    resp = await client.get(url)
    assert resp.status == 200
    data = await resp.json()

    if DEV_LOGGER.isEnabledFor(logging.DEBUG):
        DEV_LOGGER.debug("Got result:")
        for line in pformat(data).splitlines():
            DEV_LOGGER.debug("  %s", line)

    assert data["url"]["fqdn"] == url_key.fqdn
    assert data["url"]["port"] == port
    assert data["url"]["path_and_qs"] == url_key.path_and_qs

    assert data["info"]["dangerous"] == url_info.dangerous


async def assert_put_dataset(
        client,
        app,
        dataset,
        content=None,
        assert_missing=True):
    """
    Helper to put a bunch of URL records into the DB then check they appear
    correctly.

    :param content:
        Override the content of the put request.

    :param assert_missing:
        Check that records are missing (i.e. dangerous is None) before writing.
    """
    if content is None:
        content = "\n".join(
            " ".join((
                (str(url)),
                (str(info.dangerous))
            ))
            for url, info in dataset
        )
    url = app.router["url_update"].url_for()

    if assert_missing:
        # Check records are missing before we write
        for url_key, info in dataset:
            await assert_url_record_exists(
                client,
                app,
                url_key,
                URLInfo(dangerous=None),
            )

    resp = await client.put(url, data=content.encode("UTF-8"))

    assert resp.status == 200
    data = await resp.json()
    assert data["success"] is True

    # Check records are accurate
    # Could do this concurrently but for easier debugging do consecutive.
    for url_key, info in dataset:
        await assert_url_record_exists(client, app, url_key, info)


def get_url_from_url_key(app, url_key):
    """
    Generate the expected API URL to lookup info for given url key.
    """
    port = url_key.port
    if port is None:
        port_str = ""
    else:
        port_str = ":{:d}".format(port)

    return app.router["url_info"].url_for(
        fqdn=url_key.fqdn,
        port=port_str,
        path_and_qs=url_key.path_and_qs,
    )
