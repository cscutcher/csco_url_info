import warnings

from pkg_resources import DistributionNotFound
from pkg_resources import get_distribution


def get_version():
    """
    Try to discover version from pkg_resources.
    """
    try:
        version = get_distribution(__name__).version
        return version
    except DistributionNotFound:
        # package is not installed so we cant discover version
        warnings.warn(
            "Unable to discover package version. This implies package is not"
            "properly installed!")
        return None


__version__ = get_version()
