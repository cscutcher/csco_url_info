# -*- coding: utf-8 -*-
"""
Main app definition
"""
import asyncio
import os

from aiohttp import web

from csco_url_info import rest_api
from csco_url_info import test_utils
from csco_url_info.url_info_db import db_factory


def main(loop=None, run=True):
    if loop is None:
        loop = asyncio.get_event_loop()
    app = rest_api.get_app(
        loop=loop,
        db_factory=db_factory,
    )
    port_raw = os.environ.get("CSCO_LISTEN_PORT", "8080")
    port = int(port_raw)

    if run:
        web.run_app(app, port=port)
    return app


async def test_main_sanity(loop, aiohttp_client, app, test_dataset):
    """
    Sanity test main function.

    Unfortunately we can't allow `run_app` to happen as it'll block and we are
    unable to easily inject the loop, but this should provide some assurance
    that the serve CLI function is working.
    """
    app = main(loop=loop, run=False)
    client = await aiohttp_client(app)
    await test_utils.assert_put_dataset(client, app, test_dataset)
