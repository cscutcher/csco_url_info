# -*- coding: utf-8 -*-
"""
Test for REST API
"""
import logging

from yarl import URL
import pytest

from csco_url_info import __version__
from csco_url_info import test_utils
from csco_url_info.url_info import URLInfo
from csco_url_info.url_info import URLInfoKey

DEV_LOGGER = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "key,expected_url",
    [
        (
            URLInfoKey(
                fqdn="domain.com",
                port=None,
                path_and_qs="foo/bar?a=b"),
            # TODO: Note that the expected URL has encoded the QS as path.
            #       This was unintended.
            #       I'm not sure if this is a problem yet, it might actually be
            #       better if we want to use the qs to affect our lookup.
            "/urlinfo/1/domain.com/foo/bar%3Fa=b"
        ),
    ],
    ids=repr,
)
def test_get_url_from_url_key(app, key, expected_url):
    """
    Manual raw test of get to ensure auto test generation isn't out of whack.

    TODO:
        Would be better to do this raw test as part of API documentation.
    """
    assert test_utils.get_url_from_url_key(app, key) == URL(expected_url)


async def test_get(aiohttp_client, url_key_and_info, populated_app):
    """
    Check running gets on the populated database.

    Test is parameterized via `conftest.pytest_generate_tests` and will
    therefore run `conftest.PARAMETRIZED_TEST_LENGTH` times using a subset
    of the data generated via `conftest.get_test_dataset()`
    """
    app = populated_app
    url_key, url_info = url_key_and_info
    client = await aiohttp_client(app)
    await test_utils.assert_url_record_exists(client, app, url_key, url_info)


async def test_set_raw(aiohttp_client, app, url_key_and_info):
    """
    Manual raw test to ensure auto test generation isn't out of whack.

    TODO:
        Would be better to do this raw test as part of API documentation.
    """
    INPUT = """
        3k6fma6he6.p4vlfl3jgv1widhij9q9zntd37p01hq/foo/bar?a=b&c=d True
        wdilv.shzbyd1cfw.b7kvdcxjy2cbcecuostcsi5a1:35967/foo/bar?a=b&c=d False
        tvl9lgjl91ja27dn5u08z26w3elfai1xtzv7a:61085/foo/bar?a=b&c=d None
        lyrguxw9kqzjwaan49y3noisl.xn--11b5bs3a9aj6g/foo/bar?a=b&c=d False
    """

    EXPECTED = (
        (URLInfoKey(
            fqdn='3k6fma6he6.p4vlfl3jgv1widhij9q9zntd37p01hq',
            port=None,
            path_and_qs='foo/bar?a=b&c=d'),
         URLInfo(dangerous=True)),
        (URLInfoKey(
            fqdn='wdilv.shzbyd1cfw.b7kvdcxjy2cbcecuostcsi5a1',
            port=35967,
            path_and_qs='foo/bar?a=b&c=d'),
         URLInfo(dangerous=False)),
        (URLInfoKey(
            fqdn='tvl9lgjl91ja27dn5u08z26w3elfai1xtzv7a',
            port=61085,
            path_and_qs='foo/bar?a=b&c=d'),
         URLInfo(dangerous=None)),
        (URLInfoKey(
            fqdn='lyrguxw9kqzjwaan49y3noisl.xn--11b5bs3a9aj6g',
            port=None,
            path_and_qs='foo/bar?a=b&c=d'),
         URLInfo(dangerous=False)),
    )
    client = await aiohttp_client(app)
    await test_utils.assert_put_dataset(client, app, EXPECTED, INPUT)


async def test_set_single(aiohttp_client, app, url_key_and_info):
    """
    Test setting single URL records at a time using the set API.

    Test is parameterized via `conftest.pytest_generate_tests` and will
    therefore run `conftest.PARAMETRIZED_TEST_LENGTH` times using a subset
    of the data generated via `conftest.get_test_dataset()`
    """
    client = await aiohttp_client(app)
    await test_utils.assert_put_dataset(client, app, [url_key_and_info])


async def test_set_mass(aiohttp_client, app, test_dataset):
    """
    Test setting URL records on mass using the set API.

    Will run with entire dataset from `conftest.get_test_dataset()`.
    """
    client = await aiohttp_client(app)
    await test_utils.assert_put_dataset(client, app, test_dataset)


async def test_get_index(aiohttp_client, app):
    expected_data = {
        "api_version": 1,
        "version": __version__,
        "prefix": "/urlinfo/1/",
    }

    client = await aiohttp_client(app)
    resp = await client.get("/")
    assert resp.status == 200
    data = await resp.json()

    assert data == expected_data
