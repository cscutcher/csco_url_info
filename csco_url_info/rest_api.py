# -*- coding: utf-8 -*-
"""
Frontend REST API to service.
"""
from urllib.parse import unquote as url_unquote
import functools
import logging

from aiohttp import web

from csco_url_info.url_info import URLInfo
from csco_url_info.url_info import URLInfoKey
from csco_url_info.url_info import URLInfoWithKey
from csco_url_info import __version__

API_VERSION = 1
DEV_LOGGER = logging.getLogger(__name__)
URL_PREFIX = "/urlinfo/{:d}/".format(API_VERSION)


class URLInfoView(web.View):
    async def get(self):
        if self.request.match_info.get("port", ""):
            prefix, colon, port = (
                self.request.match_info["port"].partition(":"))
            assert not prefix
            assert colon == ":"
            port = int(port)
        else:
            port = None

        path_and_qs = (
            url_unquote(self.request.match_info["path_and_qs"]).lstrip("/"))

        key = URLInfoKey(
            fqdn=self.request.match_info["fqdn"],
            port=port,
            path_and_qs=path_and_qs,
        )

        info = await self.request.app["db_read"].get(key)

        key_info = URLInfoWithKey(url=key, info=info)
        return web.json_response(key_info.get_dict())

    # Borrowed from https://stackoverflow.com/a/26987741/2882435
    # We might not want to be so string on checking valid domains for
    # performance reasons, but I'll leave it in for now.
    URL_FQDN = (
        r"{fqdn:(((?!-))(xn--)?[a-z0-9-_]{0,63}[a-z0-9]{1,1}\.)*(xn--)?"
        r"([a-z0-9\-]{1,63}|[a-z0-9-]{1,30}\.[a-z]{2,})}"
    )

    URL_PORT = r"{port:(:\d+)?}"
    URL_PATH_AND_QS = r"/{path_and_qs:.*}"

    @classmethod
    def get_route_url(cls):
        return "".join((
            URL_PREFIX,
            cls.URL_FQDN,
            cls.URL_PORT,
            cls.URL_PATH_AND_QS,
        ))


class URLUpdateView(web.View):
    class DBWriteAIter:
        DANGER_STR_MAP = {
            "true": True,
            "false": False,
            "none": None
        }

        def __init__(self, content):
            self._content = content

        def __aiter__(self):
            return self

        async def __anext__(self):
            line = None
            while not line:
                if self._content.at_eof():
                    raise StopAsyncIteration
                line = await self._content.readline()
                line = line.decode("UTF-8").strip()
            url_raw, danger_raw = line.split(" ")
            key = URLInfoKey.from_str(url_raw)
            info = URLInfo(dangerous=self.DANGER_STR_MAP[danger_raw.lower()])
            DEV_LOGGER.debug("Parsed %r and got (%r, %r)", line, key, info)
            return key, info

    async def put(self):
        it = self.DBWriteAIter(self.request.content)
        await self.request.app["db_write"].upsert(it)
        return web.json_response({
            "success": True
        })

    @staticmethod
    def get_route_url():
        return URL_PREFIX


class Index(web.View):
    """
    View to be at root just so we know things are working.
    """
    async def get(self):
        return web.json_response({
            "api_version": API_VERSION,
            "version": __version__,
            "prefix": URL_PREFIX,
        })

    @staticmethod
    def get_route_url():
        return "/"


async def init_db(db_factory, app):
    db_read, db_write = await db_factory(loop=app.loop)
    app["db_read"] = db_read
    app["db_write"] = db_write


def get_app(loop, db_factory):
    app = web.Application(loop=loop)
    app.router.add_view(
        Index.get_route_url(),
        Index,
    )
    app.router.add_view(
        URLInfoView.get_route_url(),
        URLInfoView,
        name="url_info"
    )
    app.router.add_view(
        URLUpdateView.get_route_url(),
        URLUpdateView,
        name="url_update"
    )
    app.on_startup.append(functools.partial(init_db, db_factory))
    return app
