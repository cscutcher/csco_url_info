# -*- coding: utf-8 -*-
"""Pytest fixtures and session """
from itertools import islice
import functools
import logging
import os
import random

from csco_url_info import rest_api
from csco_url_info import test_utils
from csco_url_info import url_info_db
from csco_url_info.url_info import URLInfo
from csco_url_info.url_info import URLInfoKey
from csco_url_info.url_info import URLInfoWithKey

import pytest

DEV_LOGGER = logging.getLogger(__name__)

RANDOMISH_SEED = "dangermouse"


@pytest.fixture(scope="session", autouse=True)
def randomish_session():
    """ See test_utils.randomish() """
    with test_utils.randomish():
        yield


PARAMETRIZED_TEST_LENGTH = 10
GENERATED_TEST_DATA_LENGTH = 100


def get_random_url_info():
    path = test_utils.get_random_uri_path()
    qs = test_utils.get_random_uri_query_string()

    path_and_qs = "?".join(
        s for s in (path, qs) if s
    )

    url = URLInfoKey(
        fqdn=test_utils.get_random_domain_name(),
        port=test_utils.get_random_port(),
        path_and_qs=path_and_qs,
    )

    info = URLInfo(dangerous=random.choice((True, False, None)))
    return URLInfoWithKey(url, info)


@functools.lru_cache()
def get_test_dataset():
    """
    Get dataset for testing.

    Result is cached with lru_cache so all callers will get same tuple.
    """
    with test_utils.randomish():
        return tuple(
            get_random_url_info()
            for _ in range(GENERATED_TEST_DATA_LENGTH)
        )


@pytest.fixture
def test_dataset():
    """
    Fixture that returns all generated test data.
    """
    return get_test_dataset()


def pytest_generate_tests(metafunc):
    """
    Parameterize tests based on get_test_dataset and PARAMETRIZED_TEST_LENGTH
    """
    with test_utils.randomish():
        test_dataset = get_test_dataset()
        if 'url_key_and_info' in metafunc.fixturenames:
            metafunc.parametrize(
                "url_key_and_info",
                tuple(islice(test_dataset, 0, PARAMETRIZED_TEST_LENGTH)),
            )
        if 'db_factory_all' in metafunc.fixturenames:
            metafunc.parametrize(
                'db_factory_all',
                list(url_info_db.get_db_types().keys()),
                indirect=True)


@pytest.fixture
def loop(event_loop):
    """For compatibility between pytest-aiohttp and pytest-asyncio"""
    return event_loop


@pytest.fixture
def db_factory():
    return url_info_db.db_factory


@pytest.fixture
def db_factory_all(request):
    db_type = request.param
    kwargs = {"db_type": db_type}
    if db_type == "RedisClusterDatabase":
        if "CSCO_REDIS_CLUSTER" not in os.environ:
            request.applymarker(pytest.mark.xfail(
                reason="Redis not configured."))
        kwargs["flush"] = True
    return functools.partial(url_info_db.db_factory, **kwargs)


@pytest.fixture
def app(loop, db_factory):
    return rest_api.get_app(loop=loop, db_factory=db_factory)


@pytest.fixture
def populated_db_factory(loop, db_factory):
    """
    Create db for testing, which is prepopulated with contents of
    `get_test_dataset()`
    """
    async def testing_db_factory(*args, **kwargs):
        db_read, db_write = await db_factory(loop=loop)
        await db_write.upsert(get_test_dataset())
        return db_read, db_write
    return testing_db_factory


@pytest.fixture
def populated_app(loop, populated_db_factory):
    return rest_api.get_app(loop=loop, db_factory=populated_db_factory)
