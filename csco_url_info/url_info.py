# -*- coding: utf-8 -*-
"""
Types used to store URL info
"""
from collections import namedtuple
import logging
from urllib.parse import urlsplit


DEV_LOGGER = logging.getLogger(__name__)


_URLInfoKey = namedtuple("URLInfoKey", [
    "fqdn",
    "port",
    "path_and_qs",
])


class URLInfoKey(_URLInfoKey):
    """
    Class to store the URL ready for lookup in database.

    :param path_and_qs:
        Combined path and query string.
        Should never start with '/'

    :param port:
        Int or None

    :param fqdn:
        FQDN for URL.
    """
    __slots__ = ()
    get_dict = _URLInfoKey._asdict

    def __str__(self):
        """
        Convert to a string suitable for database insertion.

        TODO: This shouldn't live  in this class as it's database specific.
        """
        if self.port is None:
            port_str = ""
        else:
            port_str = ":{:d}".format(self.port)
        return f"{self.fqdn}{port_str}/{self.path_and_qs}"

    @classmethod
    def from_str(cls, s):
        """
        Convert from a string retrieved from db to inst.

        TODO: This shouldn't live  in this class as it's database specific.
        """
        url = urlsplit("".join(("//", s)))
        if url.query:
            path_and_qs = "?".join((url.path, url.query))
        else:
            path_and_qs = url.path
        if url.port is None:
            fqdn = url.netloc
        else:
            fqdn, colon, port = url.netloc.partition(":")
            assert colon and port

        return cls(
            fqdn=fqdn,
            port=url.port,
            path_and_qs=path_and_qs.lstrip("/"))


_URLInfo = namedtuple("URLInfo", ["dangerous"])


class URLInfo(_URLInfo):
    """
    Class to store info on URL to be stored in DB.

    :param dangerous:
        True, False or None. None indicates we have no information on this URL.
    """
    __slots__ = ()
    get_dict = _URLInfo._asdict

    FROM_STRING_MAP = {
        "none": None,
        "true": True,
        "false": False,
    }
    TO_STRING_MAP = {
        value: key for key, value in FROM_STRING_MAP.items()
    }

    def __str__(self):
        """
        Convert to a string suitable for database insertion.

        TODO: This shouldn't live  in this class as it's database specific.
        """
        return self.TO_STRING_MAP[self.dangerous]

    @classmethod
    def from_str(cls, s):
        """
        Convert from a string retrieved from db to info class.

        TODO: This shouldn't live  in this class as it's database specific.
        """
        dangerous = cls.FROM_STRING_MAP[s]
        return cls(dangerous=dangerous)


_URLInfoWithKey = namedtuple("URLInfoWithKey", ["url", "info"])


class URLInfoWithKey(_URLInfoWithKey):
    __slots__ = ()

    def get_dict(self):
        return {
            "url": self.url.get_dict(),
            "info": self.info.get_dict()
        }
