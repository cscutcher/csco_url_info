# -*- coding: utf-8 -*-
"""
Store implementations of URL DB
"""
from abc import ABC
from abc import abstractmethod
from concurrent.futures import ThreadPoolExecutor
import asyncio
import functools
import logging
import os

from rediscluster import StrictRedisCluster

from csco_url_info.url_info import URLInfo

DEV_LOGGER = logging.getLogger(__name__)


class IURLDatabaseRead(ABC):
    @abstractmethod
    def get(self, key):
        """
        Get info on URL from DB.

        :param key:
            URLInfoKey

        :returns:
            Awaitable that fires with a URLInfo object
        """


class IURLDatabaseWrite(ABC):
    @abstractmethod
    def upsert(self, it):
        """
        Update and insert URLInfo into DB

        :param it:
            Iterable which yields (URLInfoKey, URLInfo) pairs.

        :returns:
            awaitable which fires when upsert is complete.
        """


async def wrap_to_aiter(it):
    """Ensure that iterator is async."""
    if hasattr(it, "__aiter__"):
        async for i in it:
            yield i
    else:
        for i in it:
            yield i


class DumbDatabase(IURLDatabaseRead, IURLDatabaseWrite):
    DEFAULT_INFO = URLInfo(dangerous=None)
    LOGGER = DEV_LOGGER.getChild(__name__)

    def __init__(self, loop):
        self._loop = loop
        self._db = {}

    async def get(self, key):
        result = self._db.get(key, None)
        if result is None:
            self.LOGGER.debug("No result for %r. Returning default.", key)
            return self.DEFAULT_INFO
        return result

    async def upsert(self, it):
        it = wrap_to_aiter(it)
        async for url, info in it:
            self.LOGGER.debug("Adding %r: %r", url, info)
            self._db[url] = info


class StringDictDB(IURLDatabaseRead, IURLDatabaseWrite):
    """
    Use dict, but encode key and value as string like we do with Redis.
    """
    LOGGER = DEV_LOGGER.getChild(__name__)

    def __init__(self, loop):
        self._loop = loop
        self._db = {}

    async def get(self, key):
        return URLInfo.from_str(self._db[str(key)])

    async def upsert(self, it):
        it = wrap_to_aiter(it)
        async for url, info in it:
            self.LOGGER.debug("Adding %r: %r", url, info)
            self._db[str(url)] = str(info)


def get_db_types():
    """
    Get available db types for easy lookup by tests and on startup.

    Note:
        This ignores the split between IURLDatabaseRead and IURLDatabaseWrite
        and assumes all classes implementing the former implement the latter.

        I stand by the decision to keep the interfaces seperate, but since the
        above assumption stands I'm using it here to simplify things.
    """
    return {
        cls.__name__: cls
        for cls in IURLDatabaseRead.__subclasses__()
    }


class RedisClusterDatabase(IURLDatabaseRead, IURLDatabaseWrite):
    LOGGER = DEV_LOGGER.getChild(__name__)

    def __init__(self, loop, startup_nodes=None, flush=False):
        self._loop = loop
        self._executor = ThreadPoolExecutor(
            thread_name_prefix="url_info_redis_cluster_")

        if startup_nodes is None:
            startup_nodes_list_raw = os.environ["CSCO_REDIS_CLUSTER"]
            startup_nodes_raw = startup_nodes_list_raw.split(",")
            startup_nodes = [
                {"host": host, "port": port}
                for host, port in map(
                    lambda s: s.rsplit(":"), startup_nodes_raw)
            ]
        self.LOGGER.info(
            "Connecting to redis cluster nodes: %r", startup_nodes)

        password = os.environ.get("CSCO_REDIS_PASS", None)

        self._db = StrictRedisCluster(
            decode_responses=True,
            startup_nodes=startup_nodes,
            password=password,
        )

        if flush:
            self._db.flushall()

    def _run(self, func, *args, **kwargs):
        func = functools.partial(func, *args, **kwargs)
        return self._loop.run_in_executor(self._executor, func)

    async def get(self, key):
        key_str = str(key)
        raw_result = await self._run(self._db.get, key_str)
        if raw_result is None:
            raise KeyError(key)
        info = URLInfo.from_str(raw_result)
        return info

    async def upsert(self, it):
        it = wrap_to_aiter(it)
        p = self._db.pipeline()
        async for url, info in it:
            self.LOGGER.debug("Adding %r: %r", url, info)
            p.set(str(url), str(info))
        await self._run(p.execute)


async def db_factory(*ignored, db_type=None, **kwargs):
    """
    Primative way to switch DB implementations.


    Return two items as we might want different implementations of the read
    versus the write.

    :return:
        Tuple of IURLDatabaseRead, IURLDatabaseWrite
    """
    if ignored:
        raise RuntimeError("db_factory should only be passed kwargs")

    if db_type is None:
        db_type = os.environ.get("CSCO_URL_INFO_DB", "DumbDatabase")

    db_types = get_db_types()
    fact = db_types[db_type]
    inst = fact(**kwargs)
    return inst, inst


async def test_set_get_mass(db_factory_all, loop, test_dataset):
    """ Test all databases setting data on mass then reading back. """
    db_read, db_write = await db_factory_all(loop=loop)

    await db_write.upsert(iter(test_dataset))

    # Could do this concurrently but for easier debugging do consecutive.
    for url_key, info in test_dataset:
        assert info == (await db_read.get(url_key))


def test_benchmark_dbs(db_factory_all, loop, test_dataset, benchmark):
    """ Benchmark databases against each other. """
    db_read, db_write = loop.run_until_complete(db_factory_all(loop=loop))

    async def async_run_test():
        await db_write.upsert(iter(test_dataset))

        # Could do this concurrently but for easier debugging do consecutive
        # and more consistent for benchmark.
        for url_key, info in test_dataset:
            assert info == (await db_read.get(url_key))

    def run_test():
        fut = asyncio.ensure_future(async_run_test(), loop=loop)
        loop.run_until_complete(fut)

    benchmark(run_test)


async def test_set_single(db_factory_all, url_key_and_info, loop):
    """
    Test setting single URL records at a time on databases.

    Test is parameterized via `conftest.pytest_generate_tests` and will
    therefore run `conftest.PARAMETRIZED_TEST_LENGTH` times using a subset
    of the data generated via `conftest.get_test_dataset()`
    """
    url_key, url_info = url_key_and_info
    db_read, db_write = await db_factory_all(loop=loop)

    await db_write.upsert(iter((url_key_and_info,)))

    assert url_info == (await db_read.get(url_key))
